/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.mpfn.basiclogicweblogic.bean;

import java.util.Date;

/**
 *
 * @author Usuario
 */
public class AccesoIpBean {

    private String ipAcceso;
    private String deUsuario;
    private Date feUseMod;
    private int nuIntento;
    private String deCaptcha;
    private int inLogin;

    public String getIpAcceso() {
        return ipAcceso;
    }

    public void setIpAcceso(String ipAcceso) {
        this.ipAcceso = ipAcceso;
    }

    public String getDeUsuario() {
        return deUsuario;
    }

    public void setDeUsuario(String deUsuario) {
        this.deUsuario = deUsuario;
    }

    public Date getFeUseMod() {
        return feUseMod;
    }

    public void setFeUseMod(Date feUseMod) {
        this.feUseMod = feUseMod;
    }

    public int getNuIntento() {
        return nuIntento;
    }

    public void setNuIntento(int nuIntento) {
        this.nuIntento = nuIntento;
    }

    public String getDeCaptcha() {
        return deCaptcha;
    }

    public void setDeCaptcha(String deCaptcha) {
        this.deCaptcha = deCaptcha;
    }

    public int getInLogin() {
        return inLogin;
    }

    public void setInLogin(int inLogin) {
        this.inLogin = inLogin;
    }
    
}
