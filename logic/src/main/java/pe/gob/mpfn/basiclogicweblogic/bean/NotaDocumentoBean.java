/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.mpfn.basiclogicweblogic.bean;

public class NotaDocumentoBean {
    private Long idNota;
    private Long nuEmi;
    private String nota;
    private Integer inCompletado = 0;
    private String coEmpRegistra;
    private String coEmpCompleta;
    private String deFecRegistro;
    private String deFecCompleta;
    private Integer inBorrado = 0;

    public Long getIdNota() {
        return idNota;
    }

    public void setIdNota(Long idNota) {
        this.idNota = idNota;
    }

    public Long getNuEmi() {
        return nuEmi;
    }

    public void setNuEmi(Long nuEmi) {
        this.nuEmi = nuEmi;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public Integer getInCompletado() {
        return inCompletado;
    }

    public void setInCompletado(Integer inCompletado) {
        this.inCompletado = inCompletado;
    }

    public String getCoEmpRegistra() {
        return coEmpRegistra;
    }

    public void setCoEmpRegistra(String coEmpRegistra) {
        this.coEmpRegistra = coEmpRegistra;
    }

    public String getCoEmpCompleta() {
        return coEmpCompleta;
    }

    public void setCoEmpCompleta(String coEmpCompleta) {
        this.coEmpCompleta = coEmpCompleta;
    }

    public String getDeFecRegistro() {
        return deFecRegistro;
    }

    public void setDeFecRegistro(String deFecRegistro) {
        this.deFecRegistro = deFecRegistro;
    }

    public String getDeFecCompleta() {
        return deFecCompleta;
    }

    public void setDeFecCompleta(String deFecCompleta) {
        this.deFecCompleta = deFecCompleta;
    }

    public Integer getInBorrado() {
        return inBorrado;
    }

    public void setInBorrado(Integer inBorrado) {
        this.inBorrado = inBorrado;
    }

}
