/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.mpfn.basiclogicweblogic.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SessionFilter implements Filter {

    private String urlLogin;
    private String urlIndex;
    private String urlInicio;
    private String urlExpira;
    private String mode = "DENY";

    //@Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse hRes = (HttpServletResponse) response;

        //todo: @datevid modificar este codigo luego de las pruebas respectivas
        //hRes.addHeader("X-FRAME-OPTIONS", this.mode);
        String uri = httpServletRequest.getRequestURI();
        int x = 0;
        if (uri != null) {
            x = uri.lastIndexOf("/");
            if (x > 0) {
                uri = uri.substring(x);
            }
        }

        HttpSession sesion = httpServletRequest.getSession(true);
        if (uri.equals(this.urlLogin)) {
           chain.doFilter(request, response);
        } else if (uri.equals("/logout.do")) {
            chain.doFilter(request, response);
        } else if (uri.equals("/inicioErr.do")) {
            chain.doFilter(request, response);
        } else if (uri.equals("/sesionExpira.do")) {
            chain.doFilter(request, response);
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlExpira);
            if (sesion == null) {
                dispatcher.forward(request, response);
            }else{
                chain.doFilter(request, response);
            }
        }
    }
    
    //@Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.urlLogin = filterConfig.getInitParameter("urlLogin");
        this.urlIndex = filterConfig.getInitParameter("urlIndex");
        this.urlInicio = filterConfig.getInitParameter("urlInicio");
        this.urlExpira = filterConfig.getInitParameter("urlExpira");

        if (urlLogin == null || urlLogin.trim().length() == 0) {
            //Error al cargar la url de login
            throw new ServletException("No se ha configurado URL de login");
        }
    }

    //@Override
    public void destroy() {
    }
}
