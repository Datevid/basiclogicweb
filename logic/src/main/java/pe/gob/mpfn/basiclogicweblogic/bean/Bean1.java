package pe.gob.mpfn.basiclogicweblogic.bean;

/**
 * Created by David León @datevid on 19/08/2019 14:53.
 */
public class Bean1 {
	private String message;
	private String status;
	private String data;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
