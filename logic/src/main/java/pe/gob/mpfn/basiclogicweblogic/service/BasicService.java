package pe.gob.mpfn.basiclogicweblogic.service;

import pe.gob.mpfn.basiclogicweblogic.bean.Bean1;
import pe.gob.mpfn.basiclogicweblogic.bean.NotaDocumentoBean;

import java.util.Map;

/**
 * Created by David León @datevid on 15/05/2019 12:22.
 */
public interface BasicService {
    Map<String, Object> getDato(String variable);
    Bean1 getDato2(String variable);
    NotaDocumentoBean getNota(String variable);
}
