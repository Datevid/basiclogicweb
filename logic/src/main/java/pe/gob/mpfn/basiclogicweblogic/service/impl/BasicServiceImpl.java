package pe.gob.mpfn.basiclogicweblogic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.mpfn.basiclogicweblogic.bean.Bean1;
import pe.gob.mpfn.basiclogicweblogic.bean.NotaDocumentoBean;
import pe.gob.mpfn.basiclogicweblogic.dao.BasicDao;
import pe.gob.mpfn.basiclogicweblogic.service.BasicService;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by David León @datevid on 15/05/2019 12:24.
 */
@Service("BasicService")
public class BasicServiceImpl implements BasicService {

    @Autowired
    BasicDao basicDao;

    @Override
    public Map<String, Object> getDato(String variable) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "David León Vilca");
        map.put("status", true);
        map.put("data", "{ \"name\":\"John\", \"age\":30, \"car\":null }");
        return map;
    }

    @Override
    public Bean1 getDato2(String variable) {
        Bean1 bean = new Bean1();
        bean.setMessage("este es un mensaje");
        return bean;
    }

    @Override
    public NotaDocumentoBean getNota(String variable) {
        NotaDocumentoBean dato = basicDao.getDato(variable);
        return dato;
    }
}
