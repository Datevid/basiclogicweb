package pe.gob.mpfn.basiclogicweblogic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.gob.mpfn.basiclogicweblogic.bean.Bean1;
import pe.gob.mpfn.basiclogicweblogic.bean.NotaDocumentoBean;
import pe.gob.mpfn.basiclogicweblogic.service.BasicService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by David León @datevid on 19/08/2019 12:34.
 */
@Controller(value = "BasicController")
public class BasicController {

	@Autowired
	BasicService basicService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public String main(){
		System.out.println("main");
		return "Verificación de documentos.";
	}

	@RequestMapping(value = "/convista", method = RequestMethod.GET)
	public String convista(HttpServletRequest request, Model model){
		System.out.println("convista");
		model.addAttribute("pMensaje","Hola mundo");
		//pMensaje
		return "respuesta";
	}

	@RequestMapping(value = "/sinvista", method = RequestMethod.GET)
	public String sinvista(){
		System.out.println("sinvista");
		return "sinvista";
	}

	@RequestMapping(value = "/conservice", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> conservice(){
		System.out.println("conservice");
		Map<String, Object> abc = basicService.getDato("abc");
		return abc;
	}

	@RequestMapping(value = "/conservice2", method = RequestMethod.GET)
	@ResponseBody
	public Bean1 conservice2(){
		System.out.println("conservice2");
		Bean1 abc = basicService.getDato2("abc");
		return abc;
	}

	@RequestMapping(value = "/conobject", method = RequestMethod.GET)
	@ResponseBody
	public Bean1 conobject(){
		System.out.println("conobject");
		Bean1 bean = new Bean1();

		return bean;
	}

	@RequestMapping(value = "/condao", method = RequestMethod.GET)
	@ResponseBody
	public NotaDocumentoBean condao(){
		System.out.println("condao");
		NotaDocumentoBean abc = basicService.getNota("abc");
		return abc;
	}

}
