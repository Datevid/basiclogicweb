package pe.gob.mpfn.basiclogicweblogic.dao;

import pe.gob.mpfn.basiclogicweblogic.bean.NotaDocumentoBean;

/**
 * Created by David León @datevid on 19/08/2019 14:49.
 */
public interface BasicDao {
	NotaDocumentoBean getDato(String variable);
}
