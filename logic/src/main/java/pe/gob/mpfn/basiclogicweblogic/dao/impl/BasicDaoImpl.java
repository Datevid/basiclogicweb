package pe.gob.mpfn.basiclogicweblogic.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import pe.gob.mpfn.basiclogicweblogic.bean.NotaDocumentoBean;
import pe.gob.mpfn.basiclogicweblogic.dao.BasicDao;
import pe.gob.mpfn.basiclogicweblogic.dao.SimpleJdbcDaoBase;

/**
 * Created by David León @datevid on 19/08/2019 15:06.
 */
@Repository
public class BasicDaoImpl extends SimpleJdbcDaoBase implements BasicDao{

	private static Logger logger=Logger.getLogger(BasicDaoImpl.class);

	@Override
	public NotaDocumentoBean getDato(String variable) {
		String query="select * from public.tdtv_notas where id_nota=?";
		NotaDocumentoBean nb = null;

		try {
			nb= this.jdbcTemplate.queryForObject(query, BeanPropertyRowMapper.newInstance(NotaDocumentoBean.class),new Object[]{3});
		} catch (Exception e) {
			logger.error("Error al obtener datos: ",e);
			e.printStackTrace();
		}
		return nb;
	}
}
