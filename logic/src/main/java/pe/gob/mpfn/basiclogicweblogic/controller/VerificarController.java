package pe.gob.mpfn.basiclogicweblogic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
//import pe.gob.mpfn.verificadoc.service.AccesoIpService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by David León @datevid on 25/07/2019 16:37.
 * controller del codigo seguro de verificacion
 */
@Controller(value = "VerificarController")
public class VerificarController {

	//@Autowired
	//AccesoIpService accesoIpService;

	/**
	 * ruta que Valida, Evalua y Rutea el documento antes de enviar a la ruta final
	 * @param csv
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/ver", method = RequestMethod.GET)
	public String csv(String csv,HttpServletRequest request, HttpServletResponse response, Model model){
		String ipOrigen=getClientIP(request);
		//TipoLoginBean tipoLoginBean=accesoIpService.verificaTipoLogin(ipOrigen,0);
		model.addAttribute("tipoLogin", "1");
		//model.addAttribute("captcha",tipoLoginBean.getDeCaptchaBase64());
		model.addAttribute("coHashDoc",csv);
		return "verificar/verificarDocForm0";
	}

	@RequestMapping(value = "/verificarDocProcess", method = RequestMethod.POST)
	public String csvProcess(String coHashDoc, String codun, String captcha,HttpServletRequest request, HttpServletResponse response, Model model){
		if ((coHashDoc == null || coHashDoc.isEmpty())) {

		} else {
			//String url = "http//cea3.mpfn.gob.pe/cea/doc?hash="+csv+"&token="+"abc";
			//String redirectUrl = request.getScheme() + url;
			//return "redirect:" + redirectUrl;
		}
		model.addAttribute("pMensaje", "Validando datos");
		return "respuesta";
	}

	private String getClientIP(HttpServletRequest request) {
		String xfHeader = request.getHeader("X-Forwarded-For");
		if (xfHeader == null) {
			return request.getRemoteAddr();
		}
		return xfHeader.split(",")[0];
	}

	/**
	 * ruta final del documento en internet
	 * @param hash
	 * @param token
	 * @return
	 */
	@RequestMapping(value = "/doc", method = RequestMethod.GET)
	@ResponseBody
	public String watch(String hash,String token){
		if ((hash == null || hash.isEmpty()||token == null || token.isEmpty())) {
			return "Invalid params";
		}
		return "pdf here!!!";
	}
}
