/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.mpfn.basiclogicweblogic.bean;

/**
 *
 * @author Usuario
 */
public class TipoLoginBean {
    private int tipoLogin;
    private String deCaptchaBase64;

    public int getTipoLogin() {
        return tipoLogin;
    }

    public void setTipoLogin(int tipoLogin) {
        this.tipoLogin = tipoLogin;
    }

    public String getDeCaptchaBase64() {
        return deCaptchaBase64;
    }

    public void setDeCaptchaBase64(String deCaptchaBase64) {
        this.deCaptchaBase64 = deCaptchaBase64;
    }
    
}
