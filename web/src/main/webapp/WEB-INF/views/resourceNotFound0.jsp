<jsp:root xmlns:tiles="http://tiles.apache.org/tags-tiles" version="2.0" xmlns:c="http://java.sun.com/jsp/jstl/core"
          xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:spring="http://www.springframework.org/tags"
          xmlns:fn="http://java.sun.com/jsp/jstl/functions" xmlns:form="http://www.springframework.org/tags/form">
    
  <jsp:directive.page contentType="text/html;charset=UTF-8" />
  <jsp:output omit-xml-declaration="yes" />
  <spring:message var="title" code="error_resourcenotfound_title" htmlEscape="false" />
  <div id="title" title="${title}">
    <h2>${fn:escapeXml(title)}</h2>
    <p><b>Request URI:</b> ${pageContext.errorData.requestURI}</p>
    <p>
        <spring:message code="error_resourcenotfound_problemdescription" />
    </p>
    <c:if test="${not empty exception}">
      <p>
        <h4>
          <spring:message code="exception_details" />
        </h4>
        <spring:message var="message" code="exception_message" htmlEscape="false" />
        <div id="_message" title="${message}" openPane="false">
          <c:out value="${exception.localizedMessage}" />
        </div>
        <spring:message var="stacktrace" code="exception_stacktrace" htmlEscape="false" />
        <div id="_exception" title="${stacktrace}" openPane="false">
          <c:forEach items="${exception.stackTrace}" var="trace">
            <c:out value="${trace}" />
            <br />
          </c:forEach>
        </div>
      </p>
    </c:if>
  </div>
</jsp:root>