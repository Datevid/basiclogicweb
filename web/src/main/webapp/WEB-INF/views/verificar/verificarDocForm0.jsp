<!--
©2019 MINISTERIO PÚBLICO | FISCALÍA DE LA NACIÓN
-->
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<spring:eval expression="@applicationProps['resources.version']" var="resourceVersion"/>
<spring:eval expression="@applicationProps['application.version']" var="applicationVersion" scope="request"/>
<spring:eval expression="@applicationProps['staticResourcesUrl']" var="staticImage"/>
<spring:eval expression="@applicationProps['web_socket_server']" var="wSocketServer" scope="request"/>
<spring:eval expression="@applicationProps['css.theme']" var="csstheme" scope="request"/>
<c:set var="staticResourcesURL" value="${staticImage}" scope="request"/>
<c:set var="resourceURL" value="resources-${resourceVersion}" scope="request"/>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="${resourceURL}/css/login.min.css"/>
    <title>
        <%--<spring:eval expression="@applicationProps['login.title']"/>--%>
        Verificación de documento digital
    </title>
    <script type="text/javascript">
        var pRutaContexto = "${ctx}";
        var pAppVersion = "${applicationVersion}";
        var _ws_url_server_req = '${wSocketServer}';
    </script>
</head>

<body>
<div class="vertical-center">

    <div class="container verificarDocForm">
        <div id="login-container" class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <div id="login-logo">
                <img src="${staticResourcesURL}/images/logo-escudo-horz-positivo.png"
                     style="width: 100%; height: auto"/>
            </div>
            <div id="divFrmLogin">
                <div id="wellMensajeLogin" class="well">
                    <%--<spring:eval expression="@applicationProps['login.appname']"/>--%>
                    Verificación de documento digital
                </div>

                <c:if test="${not empty errorMessage}">
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">x</span></button>
                        <ul>
                            <li>
                                <span class="ui-messages-warn-detail"><c:out value="${errorMessage}"/></span>
                            </li>
                        </ul>
                    </div>
                </c:if>
                <form id="frmLogin" action="verificarDocProcess" method="POST" autocomplete="off">
                    <div class="form-group">
                        <input type="text" maxlength="20" name="coHashDoc" value="${coHashDoc}"
                               class="form-control"
                               required="true" tabindex="1" placeholder="Ingrese codigo hash del documento">
                    </div>
                    <div class="form-group">
                        <input type="password" maxlength="255" name="codun"
                               value="${codun}" class="form-control"
                               required="true" tabindex="2" placeholder="Ingrese Codun" data-toggle="tooltip"
                               data-placement="top" data-trigger="manual" title="Ingrese Codun"
                        />
                    </div>
                    <%--<c:choose>
                        <c:when test="${tipoLogin == 1}">
                            <div class="form-group" style="text-align: center;">
                                <img src="${captcha}" id="lgCaptcha" style="width: auto; height: auto"/>
                                <a href="javascript:void(0);" style="font-size: 24px;"
                                   onclick="getCaptcha();return false;">
                                    <span class="fa fa-refresh"></span>
                                </a>
                                <input type="text" maxlength="20" name="captcha"
                                       value="" id="deCaptcha" class="form-control"
                                       required="true" tabindex="3" placeholder="Ingrese el texto de la imágen">
                            </div>
                        </c:when>
                    </c:choose>--%>
                    <div class="form-group">
                        <input type=submit id="btn-login-action" href="#" class="btn btn-info ancho-full"
                               value="Ver documento digital"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<%--<script type="application/javascript">
    function getCaptcha() {
        jQuery.ajax({
            url: "login.do?accion=captchaLogin",
            type: 'POST',
            dataType: 'text',
            success: function (data, textStatus, jqXHR) {
                if (data) {
                    jQuery("#lgCaptcha").attr('src', data);
                }
            }
        });
    }
</script>--%>
<%--<script src="${resourceURL}/js/v2/common.min.js"></script>--%>
<%--<script src="${resourceURL}/js/v2/login.min.js"></script>--%>

</body>

</html>