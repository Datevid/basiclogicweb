<jsp:root xmlns:tiles="http://tiles.apache.org/tags-tiles" version="2.0" xmlns:c="http://java.sun.com/jsp/jstl/core"
          xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:spring="http://www.springframework.org/tags"
          xmlns:fn="http://java.sun.com/jsp/jstl/functions" xmlns:form="http://www.springframework.org/tags/form">
    
  <jsp:directive.page contentType="text/html;charset=UTF-8" />
  <jsp:output omit-xml-declaration="yes" />
        <h2>${name}</h2>
        <p>${url}</p>
        <p>
            <h4>Detalle de Error</h4>
            <c:out value="${msg}" />
        </p>
        <c:if test="${not empty exception}">
            <p>
                <h4>Detalle de Error</h4>

                <div id="_exception">
                    <c:out value="${exception.localizedMessage}" />
                </div>
                <div id="_stacktrace">
                    <c:forEach items="${exception.stackTrace}" var="trace">
                      <c:out value="${trace}" />
                      <br />
                    </c:forEach>
                </div>
            </p>
        </c:if>
</jsp:root>