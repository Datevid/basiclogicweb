<jsp:root xmlns:tiles="http://tiles.apache.org/tags-tiles" version="2.0" xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:jsp="http://java.sun.com/JSP/Page"
    xmlns:spring="http://www.springframework.org/tags">
    <jsp:directive.page contentType="text/html;charset=UTF-8" />
    <jsp:output omit-xml-declaration="yes" />
    <spring:bind path="*">
        <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                <ul>
                    <c:forEach items="${status.errors.allErrors}" var="errMsgObj">
                        <li>
                            <span class="ui-messages-warn-detail"><c:out value="${errMsgObj.defaultMessage}"/></span>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </c:if>
    </spring:bind>
</jsp:root>